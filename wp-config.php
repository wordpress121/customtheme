<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'b<h@Mf}OqHo]rkAV8DcI7NO,2Ld2JH|)c[)PC.{?y9w}:&nK93L-[`km$q(n^{9e' );
define( 'SECURE_AUTH_KEY',  ']CA# @{|cOw3j>jBeHwQR|=q3MJ5?06OlRYk$k!,)Aa(]Z~` +LS,}xOaiiA2&r6' );
define( 'LOGGED_IN_KEY',    'y$#O=&=TP]2X)2@7U:FC&IJAo`&l9g5 =i~;D~}uq-N2 k#Z#bWY;8D0[4J<TZu*' );
define( 'NONCE_KEY',        'xz${E:wS^sD<$^O?SIn_l_I^>~2{p7*=zxhW3~K 6+tx|J%izjol D^X&6|>D;0!' );
define( 'AUTH_SALT',        ' >OX(`xD^VwZWAIP[znm YS%,C|y`E1Zl@Mm.KE49B|-%L!!Om@qt[,r}+# QO8]' );
define( 'SECURE_AUTH_SALT', '8fI6S*POTy2NrRAST2/?lDc:q;m!Ic2E07Y!KN,Kh%47&=48RL:v=b^@rNWFmspa' );
define( 'LOGGED_IN_SALT',   'pvQgsxqs?/y#H_fnS8Fa7!3 7pxai*o<PhCmQ>Xw4aHTg**t{SDtMak:}B[`Oz%y' );
define( 'NONCE_SALT',       'SN(;4)jl*fzn*k[>]TF0<<f|BZ G::-&1,<h.+JtugW)|!=rvFCr;zhXbWae)!vg' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
